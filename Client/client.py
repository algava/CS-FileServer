import zmq
import os
import hashlib
import sys
import time

PROXY_IP = 'localhost'
PROXY_PORT = '50010'
PART_SIZE = 1024 * 1024 * 5

def s2b(string_data):
    return string_data.encode('ascii')

def b2s(bytes_data):
    return bytes_data.decode('ascii')

class Client:

    def __init__(self):
        
        self.context = zmq.Context()

        self.proxy = self.context.socket(zmq.REQ)

        self.proxy.connect('tcp://{}:{}'.format(PROXY_IP, PROXY_PORT))

        self.userName = 'Alejandro'

    def initialize(self):

        userName = input(" USERNAME --> ")
        self.proxy.send_multipart([b'CHECK_USER', s2b(userName)])

        response = self.proxy.recv()
        if response == b'FREE_USER':
            self.proxy.send_multipart([b'NEW_USER', s2b(userName)])
            self.proxy.recv()

        self.userName = userName

    def serversAddresses(self):
        self.proxy.send_multipart([b'SERVERS_ADDRESSES'])
        return eval(self.proxy.recv().decode('ascii'))

    def serversSockets(self, serversAddresses):
        sockets = []
        for serverAddress in serversAddresses:
            socket = self.context.socket(zmq.REQ)
            socket.connect("tcp://{}".format(serverAddress))
            sockets.append(socket)
        return sockets

    def computeHash(self, bytesData):
        sha1 = hashlib.sha1()
        sha1.update(bytesData)
        return sha1.hexdigest()


    def uploadFile(self):
        print(" Uploading file...")

        dirPath = os.path.dirname(os.path.abspath(__file__))

        fileList = []
        for file in os.listdir(dirPath):
            if not (file.startswith('.') or (file == __file__)):
                fileList.append(file)

        print(" |{:^5}|{:^25}|".format('#', 'FILE NAME'))
        cont = 0
        for fileName in fileList:
            print(" |{:^5}| {:<24}|".format(cont, fileName))
            cont += 1

        fileNumber = int(input(" FileNumber --> "))


        if 0 <= fileNumber < len(fileList):

            fileName = fileList[int(fileNumber)]
            serversAddresses = self.serversAddresses()
            serversSockets = self.serversSockets(serversAddresses)

            segmentsInformation = {}

            filePart = 0

            sha1DataFile = hashlib.sha1()

            indexName = 'Index.txt'

            fileSize = os.path.getsize(fileName)
            progress = 0.0
            print('\r PROGRESS --> {:.0%}'.format(progress), end = '')
            sys.stdout.flush()


            with open(indexName, 'w') as indexFile:

                indexFile.write('FILE_NAME   |{}\n'.format(fileName))

                with open(fileName, 'rb') as file:

                    while True:

                        dataSegment = file.read(PART_SIZE)
                        sha1DataSegment = self.computeHash(dataSegment)
                        sha1DataFile.update(dataSegment)

                        indexFile.write('SHA1_SEGMENT|{}\n'.format(sha1DataSegment))

                        segmentsInformation[sha1DataSegment] = serversAddresses[filePart % len(serversAddresses)]

                        serverSocket = serversSockets[filePart % len(serversSockets)]
                        serverSocket.send_multipart([b'UPLOAD_FILE', sha1DataSegment.encode('ascii'), dataSegment])
                        serverSocket.recv()

                        filePart += 1
                        progress += (len(dataSegment) / fileSize)
                        print('\r PROGRESS --> {:.0%}'.format(progress), end = '')
                        sys.stdout.flush()

                        if len(dataSegment) < PART_SIZE: break

                indexFile.write('SHA1_FILE   |{}'.format(sha1DataFile.hexdigest()))
            print()

            with open(indexName, 'rb') as indexFile:
                dataIndex = indexFile.read()
                sha1DataIndex = self.computeHash(dataIndex)

                serverSocket = serversSockets[filePart % len(serversSockets)]
                serverSocket.send_multipart([b'UPLOAD_FILE', sha1DataIndex.encode('ascii'), dataIndex])

                indexInformation = [sha1DataIndex, serversAddresses[filePart % len(serversAddresses)]]

            indexPath = '{}/{}'.format(os.path.dirname(os.path.abspath(__file__)), indexName)
            os.remove(indexPath)

            self.proxy.send_multipart([b'UPLOAD_FINISHED', s2b(self.userName), s2b(fileName), s2b(str(segmentsInformation)), s2b(str(indexInformation))])
            
            self.proxy.recv()

            print(" File '{}' uploaded!".format(fileName))


        else:

            print(" Wrong FileNumber!")
        

    def listFiles(self):
        self.proxy.send_multipart([b'LIST_FILES', s2b(self.userName)])
        fileList = eval(self.proxy.recv())
        print(" Listing files...")
        print(" |{:^25}|{:^11}|".format('FILE NAME', 'USER TYPE'))
        for fileName, userType in fileList:
            print(" | {:<24}|{:^11}|".format(fileName, userType))

    def sha1Addresses(self, sha1List):
        self.proxy.send_multipart([b'SHA1_ADDRESSES', s2b(str(sha1List))])
        return eval(b2s(self.proxy.recv()))

    def shareFile(self):
        self.proxy.send_multipart([b'LIST_FILES', s2b(self.userName)])
        fileList = eval(self.proxy.recv())
        print(fileList)
        if fileList == []:
            print(" You do not have files!")

        else:

            print(" |{:^5}|{:^25}|{:^11}|".format('#', 'FILE NAME', 'USER TYPE'))
            cont = 0
            for fileName, userType in fileList:
                print(" |{:^5}| {:<24}|{:^11}|".format(cont, fileName, userType))
                cont += 1


            fileNumber = int(input(" FileNumber --> "))

            if 0 <= fileNumber < len(fileList):

                fileName = fileList[fileNumber][0]

                userName = input(" Share with --> ")

                self.proxy.send_multipart([b'CHECK_USER', s2b(userName)])
                if self.proxy.recv() == b'TAKEN_USER':
                    self.proxy.send_multipart([b'SHARE_FILE', s2b(self.userName), s2b(userName), s2b(fileName)])
                    self.proxy.recv()
                    print(" File '{}' shared with '{}'".format(fileName, userName))

                else:

                    print(" User '{}' does not exists".format(userName))
            else:
                print(" Wrong FileNumber!")


    def downloadFile(self):
        print(" Downloading file...")
        self.proxy.send_multipart([b'LIST_FILES', s2b(self.userName)])
        fileList = eval(self.proxy.recv())
        print(fileList)
        if fileList == []:
            print(" You do not have files!")

        else:

            print(" |{:^5}|{:^25}|{:^11}|".format('#', 'FILE NAME', 'USER TYPE'))
            cont = 0
            for fileName, userType in fileList:
                print(" |{:^5}| {:<24}|{:^11}|".format(cont, fileName, userType))
                cont += 1


            fileNumber = int(input(" FileNumber --> "))

            if 0 <= fileNumber < len(fileList):

                fileName = fileList[fileNumber][0]

                self.proxy.send_multipart([b'SHA1_INDEX', s2b(self.userName), s2b(fileName)])
                sha1Index = b2s(self.proxy.recv())

                indexAddress = self.sha1Addresses([sha1Index])[0]

                server = self.context.socket(zmq.REQ)
                server.connect('tcp://{}'.format(indexAddress))
                server.send_multipart([b'DOWNLOAD_FILE', s2b(sha1Index)])

                indexName = 'Index.txt'
                with open(indexName, 'w') as indexFile:
                    indexFile.write(b2s(server.recv()))

                server.close()

                with open(indexName, 'r') as indexFile:
                    indexData = indexFile.readlines()

                sha1List = []

                for line in indexData:

                    dataType, dataInfo = line.split('|')
                    dataInfo = dataInfo.splitlines()[0]

                    if 'FILE_NAME' in dataType:
                        realFileName = 'Downloaded-' + dataInfo

                    elif 'SHA1_SEGMENT' in dataType:
                        sha1List.append(dataInfo)

                    elif 'SHA1_FILE' in dataType:
                        sha1File = dataInfo

                sha1SegmentsAddresses = self.sha1Addresses(sha1List)

                progress = 0.0
                print('\r PROGRESS --> {:.0%}'.format(progress), end = '')
                sys.stdout.flush()
                with open(realFileName, 'ab') as file:

                    for index in range(len(sha1List)):                
                        sha1 = sha1List[index]
                        sha1Address = sha1SegmentsAddresses[index]
                        server = self.context.socket(zmq.REQ)
                        server.connect('tcp://{}'.format(sha1Address))
                        server.send_multipart([b'DOWNLOAD_FILE', s2b(sha1)])
                        file.write(server.recv())
                        server.close()
                        progress += 100.0/len(sha1List)/100.0
                        print('\r PROGRESS --> {:.0%}'.format(progress), end = '')
                        sys.stdout.flush()
                print()

                indexPath = '{}/{}'.format(os.path.dirname(os.path.abspath(__file__)), indexName)
                os.remove(indexPath)

                print(" File '{}' downloaded!".format(fileName))

            else:

                print(" Wrong FileNumber!")



    def pause(self):
        input(" Press <ENTER> to continue")


    def menu(self):
        sel = '-1'
        while sel != '0':
            os.system('clear')
            print("   --- FILE CLIENT ---")
            print(" STATUS -> Running...")
            print(" USERNAME: {}".format(self.userName))
            print(" 1. List Files")
            print(" 2. Upload File")
            print(" 3. Download File")
            print(" 4. Share File")
            print(" 0. Exit")
            sel = input(" Select --> ")

            if sel == '1':
                self.listFiles()
                self.pause()

            elif sel == '2':
                self.uploadFile()
                self.pause()

            elif sel == '3':
                self.downloadFile()
                self.pause()

            elif sel == '4':
                self.shareFile()
                self.pause()

            elif sel == '0':
                print(" STATUS -> Ended")

            else:
                print(" Wrong option!")
                self.pause()

    def run(self):
        
        self.menu()


def main():
    client = Client()
    #try:
    client.initialize()
    client.run()
    #except:
    #    print(" STATUS -> Ended")



if __name__ == '__main__':
    
    main()